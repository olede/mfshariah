# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-08-12 07:42
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Member',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('firstname', models.CharField(max_length=100)),
                ('lastname', models.CharField(max_length=100)),
                ('photo', models.ImageField(null=True, upload_to='member')),
                ('email', models.EmailField(max_length=100, null=True)),
                ('account_id', models.IntegerField(null=True)),
                ('birthdate', models.DateField()),
                ('gender', models.CharField(max_length=20)),
                ('nobile', models.IntegerField()),
                ('address', models.TextField()),
                ('city', models.CharField(max_length=50)),
                ('zip_code', models.CharField(max_length=10)),
                ('pincode', models.CharField(default=True, max_length=20, null=True)),
                ('signature', models.ImageField(null=True, upload_to='signature')),
                ('is_active', models.BooleanField(default=True)),
                ('status', models.CharField(default='UnAssigned', max_length=50, null=True)),
                ('joined_date', models.DateField(auto_now_add=True)),
                ('created_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]

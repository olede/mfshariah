from django.conf.urls import url
from .views import add_member, list_member

urlpatterns = [
	url(r'^member/add/$', add_member, name='addmember'),
	url(r'^member/list/$', list_member, name='listmember'),
]
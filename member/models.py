from django.db import models
from django.contrib.auth.models import User
# from uuid import uuid4

# Create your models here.
# def auto_account_id(member_id):
# 	kode = "1041"
# 	member_id = member_id + 1 
# 	return kode + '000' + member_id

class Member(models.Model):
	firstname = models.CharField(max_length=100)
	lastname = models.CharField(max_length=100)
	photo = models.ImageField(upload_to='member/', blank=True)
	email = models.EmailField(max_length=100, null=True)
	account_id = models.IntegerField(null=True)
	birthdate = models.DateField()
	gender = models.CharField(max_length=20)
	mobile = models.IntegerField()
	address = models.TextField()
	city = models.CharField(max_length=50)
	zip_code = models.CharField(max_length=10)
	pincode = models.CharField(max_length=20, null=True, default=True)
	signature = models.ImageField(upload_to='signature', blank=True)
	is_active = models.BooleanField(default=True)
	status = models.CharField(max_length=50, default='UnAssigned', null=True)
	created_by = models.ForeignKey(User)
	joined_date = models.DateField(auto_now_add=True)

	def __str__(self):
		return self.firstname + ' ' + self.lastname

	def get_full_name(self):
		full_name = '%s %s' % (self.firstname, self.lastname)
		return full_name.strip()

	# class Meta:
	# 	ordering = ['-created_at']



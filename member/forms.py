from django import forms
from django.conf import settings
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from .models import Member
from datetime import date

STATUS_CHOICED = (
		('UnAssigned', 'unassgined'),
		('Assigned', 'assigned'),
	)

STATUS_ACTIVE = (
		('inactive', 'Inactive'),
		('active', 'Active'),
	)

GENDER_CHOICES = (
		('female', 'Female'),
		('male', 'Male'),
	)

class MemberForm(forms.ModelForm):
	firstname = forms.CharField(widget=forms.TextInput)
	lastname = forms.CharField(widget=forms.TextInput)
	email = forms.CharField(widget=forms.EmailInput)
	account_id = forms.IntegerField(widget=forms.TextInput)
	birthdate = forms.DateField(input_formats=settings.DATE_INPUT_FORMATS, 
								localize=True)

	gender = forms.ChoiceField(choices=GENDER_CHOICES, required=False)
	mobile = forms.IntegerField(required=False, help_text='Number only i.e 0123456789')
	address = forms.CharField(widget=forms.Textarea, required=False)
	city = forms.CharField(widget=forms.TextInput)
	zip_code = forms.IntegerField(required=False, help_text='Number only i.e 52273')
	pincode = forms.CharField(required=False, 
				widget=forms.TextInput(), label='Pin Code')
	# is_active = forms.ChoiceField(choices=STATUS_ACTIVE, required=False)
	status = forms.ChoiceField(choices=STATUS_CHOICED, required=False)
	# created_by = forms.ChoiceField(choices=User, required=False)

	def __init__(self, *args, **kwargs):
		self.user = kwargs.pop("user", None)
		self.member = kwargs.pop("member", None)
		super(MemberForm, self).__init__(*args, **kwargs)
		not_required = []


	def clean_email(self):
		email = self.cleaned_data['email']
		
		try:
			email_qs = Member.objects.filter(email=email)	# for validation catch email if exist for editing
		except:
			if email_qs.exists():
				raise forms.ValidationError(_("This email was already been registered!"))
		return email

	def clean_birthdate(self):
		bd = self.cleaned_data['birthdate']
		today = date.today()
		if (bd.year  + 18, bd.month, bd.day) >= (today.year, today.month, today.day):
			raise forms.ValidationError(_("Must be at least 18 years old to registered"))
		return bd

	def clean_mobile(self):
		mobile = self.cleaned_data.get('mobile')
		try:
			if mobile is not None:
				if int(mobile):
					check_mobile = str(mobile)
					if not mobile or not(len(check_mobile) >= 10):
						raise forms.ValidationError(_("Please enter a valid 10 digit phone number"))
		except ValueError:
			raise forms.ValidationError(_("Please enter a valid phone number"))
		return mobile


	def clean_pincode(self):
		pincode = self.cleaned_data.get('pincode')
		if pincode:
			try:
				if int(pincode):
					check_pin = str(pincode)
					if not len(check_pin) == 6:
						raise forms.ValidationError(_("Please enter a valid 6 digit yout pincode"))
			except ValueError:
				raise forms.ValidationError(_("Please enter e valid pincode"))
		return pincode

	def save(self, commit=True, *args, **kwargs):
		instance = super(MemberForm, self).save(commit=False, *args, **kwargs)
		if not instance.id:
			instance.pincode = self.cleaned_data.get('pincode')
			# instance.set_password(self.cleaned_data.get('password'))
		if commit:
			instance.save()
		return instance

	class Meta:
		model = Member
		fields = ('__all__')


from django.shortcuts import render, redirect
from django.contrib import messages
from django.shortcuts import get_object_or_404
from .models import Member
from .forms import MemberForm

# Create your views here.
def add_member(request):
	form = MemberForm
	if form.method == 'POST':
		form = MemberForm(request.POST or None)
		if form.is_valid():
			member = form.save()
			messages.success(request, "member add was successfully")

	context = {
		'form':form,
	}
	return render(request, 'member/addmember.html', context)


def list_member(request):
	member = Member.objects.all()

	context = {
		'member':member,
	}
	return render(redirect, 'member/listmember.html', context)


# def detail_member(request)
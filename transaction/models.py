from django.db import models
from django.contrib.auth.models import User
from member.models import Member

# Create your models here.

class Saving(models.Model):
	member_id = models.ForeignKey(Member, related_name='member_saving')
	start_balance = models.DecimalField(max_digits=12, decimal_places=2, null=True)
	debit = models.DecimalField(max_digits=12, decimal_places=2, null=True)
	end_balance = models.DecimalField(max_digits=12, decimal_places=2, null=True)
	saving_date = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return str(self.member_id.account_id)

	def add_endbalance(self):
		self.end_balance = self.debit + self.start_balance
		return self.end_balance


class Withdraw(models.Model):
	member_id = models.ForeignKey(Member, related_name='member_withdraw')
	start_balance = models.DecimalField(max_digits=12, decimal_places=2, null=True)
	withdrawal = models.DecimalField(max_digits=12, decimal_places=2, null=True)
	end_balance = models.DecimalField(max_digits=12, decimal_places=2, null=True)
	withdraw_date = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return str(self.member_id.account_id)

	def reduce_endbalance(self):
		self.end_balance = self.start_balance - self.withdrawal
		return self.end_balance

class Loan(models.Model):
	member_id = models.ForeignKey(Member, related_name='member_loan')
	credit_funds = models.DecimalField(max_digits=12, decimal_places=2, null=True)
	margin = models.DecimalField(max_digits=3, decimal_places=2, null=True)
	loan_period = models.IntegerField()
	instalment = models.DecimalField(max_digits=12, decimal_places=2, null=True)
	margin_instalment = models.DecimalField(max_digits=12, decimal_places=2, null=True)
	total_instalment = models.DecimalField(max_digits=12, decimal_places=2, null=True)
	total_loan = models.DecimalField(max_digits=12, decimal_places=2, null=True)
	loan_date = models.DateTimeField(auto_now_add=True)
	status = models.CharField(max_length=20, blank=True)
	approved_by = models.OneToOneField(User, related_name='loan_teller')

	def __str__(self):
		return str(self.member_id.account_id)

	def margin_result(self):
		percentage = 0.1
		self.margin = percentage * self.credit_funds
		return self.margin

class Instalment(models.Model):
	loan_id = models.ForeignKey(Loan, related_name='loan_id')
	instalment_to = models.IntegerField()
	instalment_period = models.IntegerField()
	instalment_remain = models.DecimalField(max_digits=12, decimal_places=2, null=True)
	balance = models.DecimalField(max_digits=12, decimal_places=2, null=True)
	approved_by = models.OneToOneField(User, related_name='instal_teller')
	instal_date = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return str(self.loan_id.member_id.account_id)


class MemberStatus(models.Model):
	member_id = models.ForeignKey(Member, related_name='member_status')
	status = models.CharField(max_length=30)
	saving_balance = models.ForeignKey(Saving, related_name='status_saving', null=True, blank=True)
	loan_remain = models.ForeignKey(Instalment, related_name='status_loan', null=True, blank=True)

	def __str__(self):
		return str(self.id)

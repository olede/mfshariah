from django import forms
from django.utils.translation import ugettext_lazy as _
from .models import Saving, Loan, Instalment, Withdraw, MemberStatus

import decimal

d = decimal.Decimal

class SavingForm(forms.ModelForm):
	start_balance = forms.CharField(widget=forms.TextInput(attrs={'onkeyup':'sum();'}))
	debit = forms.CharField(widget=forms.TextInput(attrs={'onkeyup':'sum();'}))

	class Meta:
		model = Saving
		fields = ['member_id', 'start_balance', 'debit',
				'end_balance']


class WithdrawForm(forms.ModelForm):

	class Meta:
		model = Withdraw
		fields = [
			'member_id', 'start_balance', 
			'withdrawal', 'end_balance'
		]

class LoanForm(forms.ModelForm):

	class Meta:
		model = Loan
		fields = [
			'member_id', 'credit_funds', 'margin', 'loan_period', 
			'instalment', 'margin_instalment', 'margin_instalment', 'total_instalment',
			'total_loan', 'status', 'approved_by',
		]

class InstalmentForm(forms.ModelForm):
	class Meta:
		model = Instalment
		fields = [
			'loan_id', 'instalment_to', 'instalment_period', 
			'instalment_remain', 'balance', 'approved_by'
		]

STATUS_CHOICES = (
		('Saving', 'saving'),
		('Loan', 'loan'),
		('Saving & Loan', 'saving-loan')
	)

class MemberStatForm(forms.ModelForm):
	status = forms.ChoiceField(choices=STATUS_CHOICES, required=False)
	class Meta:
		model = MemberStatus
		fields = ['member_id', 'status', 'saving_balance', 'loan_remain']
from django.conf.urls import url
from django.contrib.auth import views as auth_views

from .views import (login, logout, admin_dashboard,
					member_list, member_detail, member_create, member_edit, member_delete,
					saving_list, saving_detail, saving_create,
					withdraw_list, withdraw_detail, withdraw_create,
					loan_list, loan_detail, loan_create,
					instal_list, instal_detail, instal_create,
					statmember_list, statmember_detail, statmember_create,
					)

urlpatterns = [
	url(r'^$', admin_dashboard, name='admin-dashboard'),
	# url(r'^dashboard$', admin_dashboard, name='admin-dashboard'),

	url(r'^login$', auth_views.login, {'template_name':'admin/auth/login.html'}, name='admin-login'),
	url(r'^logout$', auth_views.logout, {'next_page':'/dashboard/login'}, name='admin-logout'),

	url(r'^member/list/$', member_list, name='member-list'),
	url(r'^member/(?P<id>\d+)/$', member_detail, name='member-detail'),
	url(r'^member/create/$', member_create, name='member-create'),
	url(r'^member/(?P<id>\d+)/edit/$', member_edit, name='member-edit'),
	url(r'^member/(?P<id>\d+)/delete/$', member_delete, name='member-delete'),

	url(r'^saving/list/$', saving_list, name='saving-list'),
	url(r'^saving/(?P<member_id>\d+)/$', saving_detail, name='saving-detail'),
	url(r'^saving/create/$', saving_create, name='saving-create'),

	url(r'^withdraw/list/$', withdraw_list, name='withdraw-list'),
	url(r'^withdraw/(?P<member_id>\d+)/$', withdraw_detail, name='withdraw-detail'),
	url(r'^withdraw/create/$', withdraw_create, name='withdraw-create'),

	url(r'^loan/list/$', loan_list, name='loan-list'),
	url(r'^loan/(?P<member_id>\d+)/$', loan_detail, name='loan-detail'),
	url(r'^loan/create/$', loan_create, name='loan-create'),

	url(r'^instal/list/$', instal_list, name='instal-list'),
	url(r'^instal/(?P<loan_id>\d+)/$', instal_detail, name='instal-detail'),
	url(r'^instal/create/$', instal_create, name='instal-create'),

	url(r'^member/status/$', statmember_list, name='statmember-list'),
	url(r'^member/status/(?P<member_id>\d+)/$', statmember_detail, name='statmember-detail'),
	url(r'^member/status/create/$', statmember_create, name='statmember-create'),

	# url(r'^(?P<object_name>[A-Za-z]+)$', views.object_list, name='admin-object-list'),
	# url(r'^(?P<object_name>[A-Za-z]+)/(?P<object_id>\d+)$', views.object_detail, name='admin-object-detail'),
	# url(r'^(?P<object_name>[A-Za-z]+)/create$', views.object_create, name='admin-object-create'),
]
from django import forms
from django.contrib.auth.models import User, Group, Permission
from django.contrib.auth.hashers import make_password

from django.utils.translation import ugettext_lazy as _


class UserForm(forms.ModelForm):
	id = forms.IntegerField(widget=forms.HiddenInput, required=False)
	first_name = forms.CharField(required=False, widget=forms.TextInput)
	last_name = forms.CharField(required=False, widget=forms.TextInput)
	username = forms.CharField(required=False, widget=forms.TextInput)
	email = forms.CharField(widget=forms.EmailInput(), required=False)
	birthdate = forms.DateField(required=False, input_formats=['%m/%d/%Y'])
	password = forms.CharField(max_length=100, widget=forms.PasswordInput, required=False, label='User Roles')
	groups = forms.ModelMultipleChoiceField(required=False, queryset=Group.objects.all())
	user_permissions = forms.ModelMultipleChoiceField(
								required=False,
								queryset=Permission.objects.all())


	def clean_password(self):
		password = self.cleaned_data.get('password')
		if password:
			if len(password) < 8:
				raise forms.ValditionError(
					_('Password must be at least 8 characters long!'))
		return password

	def clean_email(self):
		email = self.cleaned_data.get('email')

		email_qs = User.objects.filter(email=email)
		if email_qs.exist():
			raise forms.ValditionError(_("A user with that email already exists."))
		return email

	def clean_username(self):
		existing = User.objects.filter(username__iexact=self.cleaned_data['username'])
		if existing.exists():
			raise forms.ValditionError(_("A user username already exists."))
		return self.cleaned_data['username']

	class Meta:
		model = User
		fields = ['first_name', 'last_name', 'username', 'email', 
				'password', 'is_active', 'is_staff', 
				'is_superuser', 'groups', 'user_permissions']


class UserLoginForm(forms.Form):
	username = forms.CharField(max_length=100, 
					widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Username'}), 
					required=False)
	password = forms.CharField(
					widget=forms.PasswordInput(attrs={'class':'form-control', 'placeholder':'Password'}), 
					required=False)

	def clean_login(self):
		username = self.cleaned_data.get('username')
		password = self.cleaned_data.get('password')

		if username and password:
			self.user_chache = authenticate(username=username, password=password)
			if sels.user_chache is None:
				raise forms.ValditionError(_("Please enter a correct username and password"))
			elif not sels.user_chache.is_active:
				raise forms.ValditionError(_("This account is active,"))
		return self.cleaned_data

	
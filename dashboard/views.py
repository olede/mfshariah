from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.contrib.auth import login, authenticate, logout
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.shortcuts import (render, render_to_response, redirect, get_object_or_404)
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from django.contrib.auth.models import User
from .forms import UserForm, UserLoginForm
from member.models import Member
from member.forms import MemberForm
from transaction.models import (Saving, Withdraw, Loan, 
								Instalment, MemberStatus)
from transaction.forms import (SavingForm, WithdrawForm, LoanForm, 
								InstalmentForm, MemberStatForm)

# Create your views here.

def admin_home(request):
	return redirect(reverse("admin-dashboard"))

@login_required
def admin_dashboard(request):
	title = 'Dashboard'
	user = request.user
	
	context = {
		'title':title,
		'user':user,
	}
	return render(request, 'admin/base_dashboard.html', context)

def login(request):
	form = UserLoginForm()
	next_path = request.GET.get('nex', None)
	if request.method == 'POST':
		username = request.POST.get('username')
		password = request.POST.get('password')
		user = authenticate(username=username, password=password)
		if user is not None and user.is_active:
			login(request, user)
			if next_path is not None:
				return redirect(next_path)
			return redirect(reverse('admin-dashboard'))
	return render(request, 'admin/auth/login.html')

	# if request.method == 'POST':
	# 	username = request.POST.get("username")
	# 	password = request.POST.get("password")
	# 	user_roles = request.POST.get("user_roles")
	# 	user = authenticate(username=username, password=password, user_roles=user_roles)
	# 	if user is not None:
	# 		if user.is_active and user.is_staff:
	# 			login(request, user)
	# 			data = {'error': False, "errors":"Login sussessfully"}
	# 		else:
	# 			data = {"error":True, "errors":"User is not active"}
	# 	else:
	# 		data = {"error":True, "errors":"Username and password was incorect!!!"}

	# return render(request, 'admin/auth/login.html')

def logout(request):
	logout(request)
	return redirect(reverse('admin-login'))

@login_required
def member_list(request):
	member = Member.objects.all()
	page = request.GET.get('page', 1)

	paginator = Paginator(member, 4)
	try:
		mb = paginator.page(page)
	except PageNotAnInteger:
		mb = paginator.page(1)
	except EmptyPage:
		mb = paginator.page(paginator.num_pages)

	context = {
		'member':member,
		'mb':mb,
		# 'page':page,
	}
	return render(request, 'admin/member/list.html', context)

@login_required
def member_detail(request, id, account_id=None):
	member = get_object_or_404(Member, id=id)

	try:
		savmember = Member.objects.get(account_id=account_id)
	except ObjectDoesNotExist:
		savmember = None
	context = {
		'member':member,
		# 'choice':choice,
		'savmember':savmember,
	}
	return render(request, 'admin/member/detail.html', context)

@login_required
def member_create(request):
	forms = MemberForm()
	if request.method == 'POST':
		forms = MemberForm(request.POST or None)
		if forms.is_valid():
			start_balance = request.POST.get('start_balance')
			debit = request.POST.get('debit')
			end_balance = request.POST.get('end_balance')
			sv = forms.save(commit=False)
			sv.created_by = request.user
			sv.save()
			return redirect('/dashboard/member/list/')

	context = {
		'forms':forms,
	}
	return render(request, 'admin/member/create.html', context)

@login_required
def member_edit(request, id):
	member = get_object_or_404(Member, id=id)
	form = MemberForm(request.POST or None, instance=member)
	if form.is_valid():
		member = form.save(commit=False)
		member.save()
		# form = MemberForm(request.POST, instance=member)
		# form.save()
		return redirect('/dashboard/member/list/')
	# else:
	# 	form = MemberForm(instance=member)

	context = {
		'member': member,
		'form': form,
	}
	return render(request, 'admin/member/editmember.html', context)

def member_delete(request, id=None):
	member = get_object_or_404(Member, id=id)
	member.delete()
	return redirect('/dashboard/member/list/')

@login_required
def saving_list(request):
	member = Member.objects.all().filter()
	saving = Saving.objects.order_by('member_id')
	context = {
		'saving':saving,
	}
	return render(request, 'admin/transactions/savinglist.html', context)

def savinglist_member(request, account_id):
	member = Member.objects.get(account_id=account_id)
	return render(request, 'admin/member/savingmember.html', {'member':member})

@login_required
def saving_detail(request, member_id):
	saving = get_object_or_404(Saving, id=member_id)
	context = {
		'saving':saving,
	}
	return render(request, 'admin/transactions/savingdetail.html', context)

@login_required
def saving_create(request):
	form = SavingForm()
	if request.method == 'POST':
		form = SavingForm(request.POST or None)
		if form.is_valid():
			sv = form.save(commit=False)
			sv.status = "Applied"
			sv.save()
			return redirect('/dashboard/saving/list')

	context = {
		'form':form,
	}
	return render(request, 'admin/transactions/savingcreate.html', context)

def withdraw_list(request):
	withdraw = Withdraw.objects.all()
	context = {
		'withdraw': withdraw,
	}
	return render(request, 'admin/transactions/withdrawlist.html', context)

@login_required
def withdraw_detail(request, member_id):
	withdraw = get_object_or_404(Withdraw, id=member_id)
	context = {
		'withdraw':withdraw,
	}
	return render(request, 'admin/transactions/withdrawdetail.html', context)

@login_required
def withdraw_create(request):
	form = WithdrawForm()
	if request.method == 'POST':
		form = WithdrawForm(method.POST or None)
		if form.is_valid():
			sv = form.save(commit=False)
			sv.save()
			return redirect(reverse('withdraw-list'))

	context = {
		'form':form,
	}
	return render(request, 'admin/transactions/withdrawcreate.html', context)

@login_required
def loan_list(request):
	loan = Loan.objects.all()
	context = {
		'loan':loan,
	}
	return render(request, 'admin/transactions/loanlist.html', context)

@login_required
def loan_detail(request, member_id):
	loan = get_object_or_404(Loan, id=member_id)
	context = {
		'loan':loan,
	}
	return render(request, 'admin/transactions/loan_detail.html', context)

@login_required
def loan_create(request):
	form = LoanForm()
	if request.method == 'POST':
		form = LoanForm(method.POST or None)
		if form.is_valid():
			sv = form.save(commit=save)
			sv.save()
			return redirect(reverse('loan-list'))

	context = {
		'form':form,
	}
	return render(request, 'admin/transactions/loancreate.html', context)

@login_required
def instal_list(request):
	instal = Instalment.objects.all()
	context = {
		'instal':instal,
	}
	return render(request, 'admin/transactions/installist.html', context)

@login_required
def instal_detail(request, member_id):
	instal = get_object_or_404(Instalment, id=member_id)
	context = {
		'instal':instal,
	}
	return render(request, 'admin/transactions/instaldetail.html', context)

@login_required
def instal_create(request):
	form = InstalmentForm()
	if request.method == 'POST':
		form = InstalmentForm(method.POST or None)
		if form.is_valid():
			sv = form.save(commit=False)
			sv.save()
			return redirect(reverse('instal-list'))

	context = {
		'form':form,
	}
	return render(request, 'admin/transactions/instalcreate.html', context)

@login_required
def statmember_list(request):
	status = Member.objects.all()
	context = {
		'status':status,
	}
	return render(request, 'admin/member/statmemberlist.html', context)

@login_required
def statmember_detail(request, member_id):
	status = get_object_or_404(MemberStatus, id=member_id)
	context = {
		'status':status,
	}
	return render(request, 'admin/member/statmemberdetail.html', context)

@login_required
def statmember_create(request):
	form = MemberStatForm()
	if request.method == 'POST':
		form = MemberStatForm(request.POST or None)
		if form.is_valid():
			sv = form.save(commit=False)
			sv.save()
			return redirect('dashboard/member/status')

	return render(request, 'admin/member/statmembercreate.html', {'form':form})

